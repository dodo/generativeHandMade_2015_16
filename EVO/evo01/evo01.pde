
ArrayList<MyShape> allShapes = new ArrayList<MyShape>();//list of shapes

ArrayList<MyShape> parents = new ArrayList<MyShape>();

ArrayList<PVector> genom = new ArrayList<PVector>();//globaly accessible "gene pool"

int mu=25;//mutation rate
int mu_pro=8;//mutation probability

void setup() {
  size(1900, 1000);
}

void draw() {
  background(255);


  for (int i=0; i<allShapes.size(); i++) {//display all the shapes
    MyShape s=allShapes.get(i);
    s.display();
  }
}

void mousePressed() {
  if (mouseButton==LEFT) {
    MyShape s = new MyShape(new PVector(mouseX, mouseY)); //create a random shape
    allShapes.add(s);
  }
  if (mouseButton==RIGHT) {
    MyShape s = new MyShape(new PVector(mouseX, mouseY), genom); //create a shape from current gene pool
    allShapes.add(s);
  }
}


void keyPressed() {
  if (key==' ') {
    for (MyShape s : allShapes) {
      PVector mPos=new PVector(mouseX, mouseY);

      if (s.pos.dist(mPos)<50&&parents.size()<2) {//select whatever shape is under the mouse
        parents.add(s);
      }
    }
  }

  if (key=='x') {
    for (int i=0; i<allShapes.size(); i++) {
      MyShape s=allShapes.get(i);
      PVector mPos=new PVector(mouseX, mouseY);

      if (s.pos.dist(mPos)<50) {//delete whatever shape is under the mouse
        allShapes.remove(s);
      }
    }
  }

  if (key=='e') {
    evolve();
  }
}