class MyShape {
  
  ArrayList<PVector> pts = new ArrayList<PVector>();
  PVector pos;
  

MyShape(PVector mpos){
  pos=mpos;
   for (int i=0; i<7; i++) {
    PVector p = new PVector(random(-50, 50), random(-50, 50),random(1,20));
    pts.add(p);
  } 
}


MyShape(PVector mpos, ArrayList<PVector> _genom){
  pos=mpos;
   for (int i=0; i<genom.size(); i++) {
     if(random(10)>mu_pro){
    PVector p = genom.get(i).copy().add(new PVector(random(-mu,mu),random(-mu,mu),random(-mu,mu)));
    pts.add(p);
     }
     else{
          PVector p = genom.get(i).copy();
    pts.add(p); 
     }
  } 
}


void behave(PVector mpos){
  if(pos.dist(mpos)<100){
    for (PVector p : pts) {
      p.add(new PVector(random(-1,1),random(-1,1)));
    }
  }
  
  
}


  void display() { 
    color c;
    if(parents.contains(this)){
      c=color(255,0,100);
    }
    else{
      c=color(0);
    }
    
    pushMatrix();
    translate(pos.x,pos.y);
    
    stroke(c);
    noFill();
    strokeWeight(0.5);
    beginShape();
    for (PVector p : pts) {
      vertex(p.x, p.y);
      }
    endShape();
    
    fill(255,200);
    strokeWeight(1);
    for (PVector p : pts) {
     ellipse(p.x,p.y,p.z,p.z); 
      
    }
    popMatrix();
    
  }
}