void evolve() {// evolve funcion mixes the 'pts' lists of two parent shapes
  MyShape p1 = parents.get(0);
  MyShape p2 = parents.get(1);

  genom = new ArrayList<PVector>();

if(random(10)>5){

  for (int i=0; i<p1.pts.size(); i++) {

     PVector  cpt1 = p1.pts.get(i);
     PVector  cpt2 = p2.pts.get(i);

    if (i%2==0) {

      PVector mixd = new PVector(cpt1.x, cpt2.y, cpt1.z);
      genom.add(mixd);
    } else {

      PVector mixd = new PVector(cpt2.x, cpt1.y, cpt2.z);
      genom.add(mixd);
    }
  }
}

else{
   for (int i=0; i<p1.pts.size(); i++) {

     PVector  cpt1 = p2.pts.get(i);
     PVector  cpt2 = p1.pts.get(i);

    if (i%2==0) {

      PVector mixd = new PVector(cpt1.x, cpt2.y, cpt1.z);
      genom.add(mixd);
    } else {

      PVector mixd = new PVector(cpt2.x, cpt1.y, cpt2.z);
      genom.add(mixd);
    }
  } 
}

  parents.clear();
}