import controlP5.*;

import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;
import processing.opengl.*;

PeasyCam cam;
ControlP5 cp5;

HE_Mesh mesh;
WB_Render render;
WB_SelectRender3D selrender;
HE_Selection selection;

int sx=30;
int sy=30;
int sz=30;

int cs=15;

boolean selActive=true;

void setup() {
  size(1000, 1000, P3D);
  cam=new PeasyCam(this, 1000);
  cp5 = new ControlP5(this);
  cp5.addButton("colorA")
     .setValue(0)
     .setPosition(100,100)
     .setSize(200,19)
     ;
  
  // and add another 2 buttons
  cp5.addButton("colorB")
     .setValue(100)
     .setPosition(100,120)
     .setSize(200,19)
     ;
     
  cp5.addButton("colorC")
     .setPosition(100,140)
     .setSize(200,19)
     .setValue(0)
     ;
  cp5.setAutoDraw(false);
  smooth(8);

  // Create an isosurface from an explicit grid of values.
  // Potentially uses a lot of memory.

  float[][][] values=new float[sx][sy][sz];
  for (int i = 0; i < sx; i++) {
    for (int j = 0; j < sy; j++) {
      for (int k = 0; k < sz; k++) {
        values[i][j][k]=2.1*noise(0.077*i, 0.077*j, 0.07*k);
      }
    }
  }

  HEC_IsoSurface creator=new HEC_IsoSurface();
  creator.setResolution(sx-1, sy-1, sz-1);// number of cells in x,y,z direction
  creator.setSize(cs, cs, cs);// cell size
  creator.setValues(values);// values corresponding to the grid points
  // values can also be double[][][]
  creator.setIsolevel(1);// isolevel to mesh
  creator.setInvert(false);// invert mesh
  creator.setBoundary(100);// value of isoFunction outside grid
  creator.clearBoundary();//to rest boundary values to "no value".
  // A boundary value of "no value" results in an open mesh

  mesh=new HE_Mesh(creator);
  selection=new HE_Selection(mesh);
  render=new WB_Render(this);
  selrender=new WB_SelectRender3D(this);
}

void draw() {
  background(255);
  lights();
  noStroke();
  fill(255);
  render.drawFacesFC(mesh);
  stroke(0);
  strokeWeight(0.5);
  render.drawEdges(mesh);
 if(selActive) selrender.drawFaces(mesh);
  fill(255, 0, 0);
  //Antialiasing on face edges can lead to false selection.
  //Use getKeyAA() to use a 3x3 sampling cross.
  if(selActive)render.drawFace(selrender.getKeyAA(), mesh);
  stroke(200, 0, 100);
  strokeWeight(2);
  render.drawEdges(selection);
  noLights();
  gui();
}
void gui() {
  hint(DISABLE_DEPTH_TEST);
  cam.beginHUD();
  cp5.draw();
  cam.endHUD();
  hint(ENABLE_DEPTH_TEST);
}

public void colorA(int theValue) {

    // mesh.modifySelected(new HEM_Extrude().setDistance(10).setChamfer(.5), selection);
    HEM_SmoothInset modifier=new HEM_SmoothInset();
    modifier.setLevel(2);// level of recursive division
    modifier.setOffset(2);// distance between inset face and original faces (should be > 0)
    mesh.modifySelected(modifier, selection);
    selection.clear();
  
}


public void colorB(int theValue) {
selActive=!selActive;  
}

public void colorC(int theValue) {
 
}


void keyPressed() {
  
  if(key=='s'){
   saveRaw(mesh,"glorious");  
  }
if(key=='f'){
    selection=new HE_Selection(mesh);  

  //add faces to selection 
  HE_FaceIterator fItr=new HE_FaceIterator(mesh);
  HE_Face f;
  while (fItr.hasNext ()) {
    f=fItr.next();
    if (random(100)<30) selection.add(f);
  }

  HES_CatmullClark cc=new HES_CatmullClark().setKeepEdges(false).setKeepBoundary(false);

  //only modify selection (if applicable)
  mesh.subdivideSelected(cc, selection, 1);

  //modifier try to preserve selections whenever possible


  mesh.modifySelected(new HEM_Extrude().setDistance(3).setChamfer(.5), selection);
}
  if (key=='c') {
    long currF=selrender.getKey();//get the name of the face currently under the mouse
    HE_Face f= mesh.getFaceByKey(currF);//get a vertex with that name frome 'fused'mesh
    if (f!=null) {
      f.setColor(color(random(255), random(255), 50));
    }
  }

  if (key=='x') {
    long currF=selrender.getKey();//get the name of the face currently under the mouse
    HE_Face f= mesh.getFaceByKey(currF);//get a vertex with that name frome 'fused'mesh
    if (f!=null) {
      mesh.removeFace(mesh.getIndex(f));
    }
  }
  if (key==' ') {
    long currF=selrender.getKey();//get the name of the face currently under the mouse
    HE_Face f= mesh.getFaceByKey(currF);//get a vertex with that name frome 'fused'mesh
    if (f!=null) {//only if there is such vertex, do the line below ( sometimes you might point somewhere in the space, where there are no valid vertices, always good to make sure
      selection.add(f);
    }
  }
  if (key=='l') {
    HEM_Smooth modifier=new HEM_Smooth();
    modifier.setIterations(3);
    modifier.setAutoRescale(true);// rescale mesh to original extents
    mesh.modify(modifier);
    mesh.validate();
  }
  if (key=='r') {
    mesh.simplify(new HES_TriDec().setGoal(mesh.getNumberOfFaces()/3));
    mesh.validate();
  }
  if (key=='d') {
    mesh.modify( new HEM_Diagrid());
    mesh.validate();
  }
}