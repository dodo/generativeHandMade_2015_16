import controlP5.*;

import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;
import processing.opengl.*;

HE_Mesh mesh;
WB_Render render;

PeasyCam cam;
ControlP5 cp5;



int gw=50;
int gh=50;
int gd=50;

PImage picture[]=new PImage[gd];

int cs=10;

float nscl=0.07;
float nsMult=5;

WB_Plane P;//section plane

void setup() {
  size(1900, 1000, P3D);
  cam=new PeasyCam(this, 1000);
  cp5 = new ControlP5(this);
  cp5.addButton("smoothIt").setPosition(100, 100).setSize(50, 50);
  cp5.addButton("makeIt").setPosition(100, 160).setSize(50, 50);
  cp5.addButton("sliceIt").setPosition(160, 160).setSize(50, 50);
  cp5.addButton("reduceIt").setPosition(160, 100).setSize(50, 50);
  cp5.addSlider("nscl").setRange(0.01, 0.5).setPosition(100, 280);
  cp5.addSlider("nsMult").setRange(1, 100).setPosition(100, 300);
  cp5.addSlider("spz").setRange(-100, 100).setPosition(100, 320);
  cp5.setAutoDraw(false);
  smooth(8);

  P=new WB_Plane(0, 0, 0, 0, 0, 1);

  for (int i=0; i<gd; i++) {
    //picture[i] = loadImage("jelitsch01.jpg");
    picture[i] = loadImage("sfrm- ("+(i+1)+").jpg");
  }

  // Create an isosurface from an explicit grid of values.
  // Potentially uses a lot of memory.

  float[][][] values=new float[gw][gh][gd];
  for (int i = 0; i < gw; i++) {
    for (int j = 0; j < gh; j++) {
      for (int k = 0; k < gd; k++) {
        values[i][j][k]=1.5*map(brightness(picture[k].get(i, j)), 0, 255, 0, 1);
        //values[i][j][k]=2.1*noise(0.07*i, 0.07*j, 0.07*k);
        //values[i][j][k]=(sin(i)+sin(j)+sin(k))*2;
      }
    }
  }

  HEC_IsoSurface creator=new HEC_IsoSurface();
  creator.setResolution(gw-1, gh-1, gd-1);// number of cells in x,y,z direction
  creator.setSize(cs, cs, cs);// cell size
  creator.setValues(values);// values corresponding to the grid points
  // values can also be double[][][]
  creator.setIsolevel(1);// isolevel to mesh
  creator.setInvert(false);// invert mesh
  creator.setBoundary(100);// value of isoFunction outside grid
  //creator.clearBoundary();// to rest boundary values to "no value".
  // A boundary value of "no value" results in an open mesh

  mesh=new HE_Mesh(creator);

  render=new WB_Render(this);
}

void draw() {
  background(120);
  lights();
  noStroke();
  strokeWeight(0.5);
  fill(255);
  render.drawFaces(mesh);
  stroke(0);
  render.drawEdges(mesh);
  noFill();
  render.drawPlane(P, 300);
  strokeWeight(4);
  stroke(255, 0, 155);
  render.drawEdgesWithInternalLabel(1, mesh);
  noLights();
  gui();
}

void smoothIt() {
  HEM_Smooth modifier=new HEM_Smooth();
  modifier.setIterations(4);
  modifier.setAutoRescale(true);// rescale mesh to original extents
  mesh.modify(modifier);
}

void makeIt() {

  float[][][] values=new float[gw][gh][gd];
  for (int i = 0; i < gw; i++) {
    for (int j = 0; j < gh; j++) {
      for (int k = 0; k < gd; k++) {
        values[i][j][k]=1.5*map(brightness(picture[k].get(i, j)), 0, 255, 0, 1);
        //values[i][j][k]=2.1*noise(0.07*i, 0.07*j, 0.07*k);
        //values[i][j][k]=(sin(i)+sin(j)+sin(k))*2;
      }
    }
  }

  HEC_IsoSurface creator=new HEC_IsoSurface();
  creator.setResolution(gw-1, gh-1, gd-1);// number of cells in x,y,z direction
  creator.setSize(cs, cs, cs);// cell size
  creator.setValues(values);// values corresponding to the grid points
  // values can also be double[][][]
  creator.setIsolevel(1);// isolevel to mesh
  creator.setInvert(false);// invert mesh
  creator.setBoundary(100);// value of isoFunction outside grid
  //creator.clearBoundary();// to rest boundary values to "no value".
  // A boundary value of "no value" results in an open mesh

  mesh=new HE_Mesh(creator);
}

void sliceIt() {
  HEM_Slice modifier=new HEM_Slice();
  modifier.setPlane(P);// Cut plane 
  //you can also pass directly as origin and normal:  modifier.setPlane(0,0,-200,0,0,1)
  modifier.setOffset(0);// shift cut plane along normal
  modifier.setCap(false);// cap holes, hemesh doesn't handle nested faces yet (e.g. when slicing a donut along its equator)
  modifier.setKeepCenter(false);// retain center of original mesh
  modifier.setReverse(true);// keep other side of plane
  mesh.modify(modifier);
  mesh.triangulate();
}

void reduceIt() {

  mesh.simplify(new HES_TriDec().setGoal(mesh.getNumberOfFaces()/2));
  mesh.validate();
}

void spz(float val) {
  P=new WB_Plane(0, 0, val, 0, 0, 1);
}

void gui() {
  hint(DISABLE_DEPTH_TEST);
  cam.beginHUD();
  cp5.draw();
  cam.endHUD();
  hint(ENABLE_DEPTH_TEST);
}