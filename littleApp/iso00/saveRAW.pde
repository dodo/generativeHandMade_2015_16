PrintWriter output;

void saveRaw(HE_Mesh toSave, String filename) {

  HE_Face f;
  HE_Vertex v;
  output = createWriter(filename+".raw");

  int indices[]=new int[4];

  println(toSave.getNumberOfFaces());
  for (int i=0; i<toSave.getNumberOfFaces(); i++) {

    f=toSave.getFaceByIndex(i);
    int vcount=0;
    HE_FaceVertexCirculator fvCrc=new HE_FaceVertexCirculator(f);
    while (fvCrc.hasNext ()) {
      v=fvCrc.next();
      indices[vcount]=toSave.getIndex(v);
      vcount++;
      //output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
    }
    if (vcount==4) {
      v= toSave.getVertexByIndex(indices[0]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      v= toSave.getVertexByIndex(indices[1]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      v= toSave.getVertexByIndex(indices[2]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      output.println();
      v= toSave.getVertexByIndex(indices[0]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      v= toSave.getVertexByIndex(indices[2]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      v= toSave.getVertexByIndex(indices[3]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      output.println();
    } else if (vcount==3) {
      v= toSave.getVertexByIndex(indices[0]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      v= toSave.getVertexByIndex(indices[1]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      v= toSave.getVertexByIndex(indices[2]);
      output.print(v.xf()+" "+v.yf()+" "+v.zf()+" ");
      output.println();
    } else {
      println(vcount);
    }
  }
}