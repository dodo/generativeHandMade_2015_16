
PGraphics bckg;


ArrayList dots = new ArrayList();



void setup() {
  size(1000, 1000);
  bckg= createGraphics(width,height);
  bckg.beginDraw();
  bckg.background(255);
  bckg.endDraw();
    
  
  
  for(int i=0;i<10;i++){
    dots.add(new Particle());
  }
  
}


void draw() {
  background(bckg);


  for (int i=0;i<dots.size();i++){
    Particle p = (Particle)dots.get(i);
    p.behave(); 
    p.display();
  }
}


void mouseDragged(){
  if(mouseButton==LEFT){
 dots.add(new Particle(new PVector(mouseX,mouseY),true)); 
  }
  else if(mouseButton==RIGHT){
    dots.add(new Particle(new PVector(mouseX,mouseY),false));  
  }
}