class Particle {

  PVector pos;
  boolean isActive;
  PVector vel = new PVector(random(-1, 1), random(-1, 1));


  Particle() {
    pos=  new PVector(random(width), random(height));
    isActive=true;
  }

  Particle(PVector iPos, boolean _isActive) {
    pos=iPos;
    isActive=_isActive;
  }


  void behave() {

    vel.setMag(1);
    pos.add(vel);


    color reading = bckg.get((int)pos.x, (int)pos.y);
    if (brightness(reading)<250) {
      isActive=false;
    }
  }

  void display() {
    if (isActive) {
      strokeWeight(3);
      point(pos.x, pos.y);
    } else {
      bckg.beginDraw();

      
      for (int i=0; i<5; i++) {
        PVector fin=new PVector(random(-30, 30), random(-30, 30));
        bckg.stroke(0, 50);
        bckg.strokeWeight(1);
        bckg.line(pos.x, pos.y, pos.x+fin.x, pos.y+fin.y);
        bckg.stroke(0, 100);
        bckg.strokeWeight(3);
        bckg.point(pos.x+fin.x, pos.y+fin.y);
      }
      bckg.endDraw();
      dots.remove(this);
    }
  }
}