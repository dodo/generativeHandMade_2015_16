class Particle {
  PVector pos=new PVector(500, 500);
  PVector vel=new PVector(random(-1, 1), random(-1, 1));
  PVector acc=new PVector(0, 0);
  color c;
  float s;
  float speedLimit;
  float accMag;

  boolean isActive;

  Particle() {
    c=color(random(255), 0, random(100));
    isActive=true;
    s=random(2,10);
    speedLimit=random(2,5);
    accMag=random(0.5,1.5);
  }

  Particle(PVector _mousePos, int tp) {
    isActive=true;
    pos=_mousePos; 
    if(tp==1){
    c=color(random(55), random(255), random(200));
    s=random(2,10);
    speedLimit=random(1,3);
    accMag=random(2);
    }
    else if(tp==2){
     c=color(random(255), random(255), random(20));
    s=random(3,5); 
    speedLimit=random(0.5,4);
    accMag=random(1,2);
    }
  }

  Particle(PVector _mousePos, boolean _isActive) {
    isActive=_isActive;
    pos=_mousePos; 
    c=color(random(55), random(255), random(200));
    s=random(2,10);
    speedLimit=random(4,5);
    accMag=1;
  }

  void behave() {
    if (isActive) {
      if (frameCount%10==0) {
        acc =new PVector(random(-1, 1), random(-1, 1));
      }
      acc.setMag(accMag);
      vel.add(acc);
      vel.limit(speedLimit);
      pos.add(vel);
      int reading=cnvs.get((int)pos.x, (int)pos.y);
      if (brightness(reading)<200) {
        isActive=false;
      }
    }
    if(pos.x>=width-5||pos.x<=5||pos.y>=height-5||pos.y<=5){
      allParticles.remove(this);
    }
  }

  void display() {
    if (isActive) {
      stroke(c);
      strokeWeight(2);
      point(pos.x, pos.y);
    } else {
      cnvs.beginDraw();
      cnvs.fill(c,50);
      cnvs.stroke(0,60);
      cnvs.strokeWeight(1);
      cnvs.ellipse(pos.x, pos.y, s, s);
      cnvs.endDraw();
      allParticles.remove(this);
    }
  }
}