
ArrayList allParticles = new ArrayList();

PGraphics cnvs;

void setup() {
  size(1000, 1000);
  cnvs=createGraphics(width,height);
  cnvs.beginDraw();
  cnvs.background(255);
  cnvs.endDraw();
  

init();
}


void draw() {
  background(cnvs);

  for (int i=0; i<allParticles.size(); i++) {
    Particle p = (Particle)allParticles.get(i);
    p.behave();
    p.display();
  }
}


void mouseDragged() {
  if(mouseButton==LEFT){
  PVector mousePos = new PVector(mouseX,mouseY);
  Particle p=new Particle(mousePos,1);
  allParticles.add(p);  
  }
  
    if(mouseButton==RIGHT){
  PVector mousePos = new PVector(mouseX,mouseY);
  Particle p=new Particle(mousePos,2);
  allParticles.add(p);
  }
  
  if(mouseButton==CENTER){
     PVector mousePos = new PVector(mouseX,mouseY);
  Particle p=new Particle(mousePos,false);
  allParticles.add(p); 
  }
}

void init(){
  for (int i=0; i<1000; i++) {
    Particle p=new Particle();
    allParticles.add(p);
  }  
}