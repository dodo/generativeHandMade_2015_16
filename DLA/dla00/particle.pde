class Particle {


  PVector pos;
  boolean isActive;
  float s;
  color c= color(random(100, 255), random(200, 255), random(100, 255));


  Particle(boolean _isActive) {
    pos=new PVector(random(100, 900), random(100, 900));
    isActive=_isActive;
    s=random(5, 15);
  }

  Particle(PVector iPos, boolean _isActive) {
    pos=iPos;
    isActive=_isActive;
    s=random(7, 15);
  }

  void behave() {
    if (isActive) {//only if the particle is active...

      PVector vel=new PVector(random(-3, 3), random(-3, 3));//change the velocity
      pos.add(vel);//and move it around
      color reading=bckg.get((int)pos.x, (int)pos.y);//read from the 'bckg' buffer, what is below the particle (pixel value)
      if (brightness(reading)<190) {//if that pixel is darker than white..
        isActive=false;//change the particle state
      }
    }
  }

  void display() {
    if (isActive) {//if the particle is active
      stroke(0, 20);
      strokeWeight(2);
      point(pos.x, pos.y);//display it as a point
    } else {//otherwise
      bckg.beginDraw();
      bckg.imageMode(CENTER);
      bckg.tint(c);
      bckg.image(self, pos.x, pos.y, s, s);//draw it as an image, we loaded in the beginning
      bckg.endDraw();
      dots.remove(this);//and remove it from the list
    }
  }
}