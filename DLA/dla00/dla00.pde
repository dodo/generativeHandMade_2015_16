
PImage self;//name for the image, that we will use for all the particles

PGraphics bckg;//graphics buffer (additional canvas) to easily draw and take information from, check in the reference

ArrayList dots = new ArrayList();//list for all the particles


void setup() {
  size(1000, 1000);

  bckg=createGraphics(width, height);//create a graphics buffer with the size of our sketch
  bckg.beginDraw();
  bckg.background(255);
  bckg.endDraw();

  self=loadImage("self_0.png");//load the image for particles. it has to be in 'data' folder of this sketch

  for (int i=0; i<10000; i++) {
    dots.add(new Particle(true));//add 10000 particles to our list
  }
}


void draw() {
  background(bckg);

  for (int i=0; i<dots.size(); i++) {//for the number of items in 'dots'
    Particle p=(Particle)dots.get(i);//get a particle from that list
    p.behave();//and make it do things
    p.display();
  }
}



void mouseDragged() {
  if (mouseButton==LEFT) {
    dots.add(new Particle(new PVector(mouseX, mouseY), true));//left button adds 'active' particles, check the boolean 'true' in declaration and compare it with contents of Particle class
  }
  if(mouseButton==RIGHT){
    dots.add(new Particle(new PVector(mouseX, mouseY), false));//right button adds static particles. other ones will stick to them
  }
}