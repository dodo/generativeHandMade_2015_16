import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

import processing.dxf.*;
import ddf.minim.analysis.*;
import ddf.minim.*;
FFT fftLin;
FFT fftLog;

Waveform myRects;

Minim minim;
AudioPlayer groove;

boolean record;

PFont font;

float camzoom;
float maxX = 0;float maxY = 0;float maxZ = 0;
float minX = 0;float minY = 0;float minZ = 0;


void setup(){
  size(1900,1000,P3D);
  PeasyCam cam=new PeasyCam(this,1000);
  noStroke();
  minim = new Minim(this);
  groove = minim.loadFile("naked.mp3");
  //repeat the song
  groove.loop();
  background(255);

  font = loadFont("HelveticaNeue-Bold-18.vlw");

  fftLog = new FFT(groove.bufferSize(),groove.sampleRate());
  fftLog.logAverages(24,2);     //adjust numbers to adjust spacing

  float w = float (width/fftLog.avgSize());
  float x = w; /// "w" for wave scribt
  float y = 50;
  float z = 1;
  float radius = 2;  
  myRects = new Waveform(x,y,z,radius);
}


void draw(){
  background(0);
  directionalLight(126,126,126,sin(radians(frameCount)),cos(radians(frameCount)),1);
  ambientLight(102,102,102);


  //play the song
  fftLog.forward(groove.mix);

  myRects.update();
  //myRects.textdraw();

  if(record){
    beginRaw(DXF, "output.dxf");
  }
  // DXF will export the stuff drawn between here.

  myRects.plotTrace();



  if(record){
    endRaw();
    record = false;
    println("Done DXF~!");
  }
}


void stop() {
  // always close Minim audio classes when you finish with them
  groove.close();
  // always stop Minim before exiting
  minim.stop();
  super.stop();
}