class Waveform{
  float x,y,z;
  float radius;

  PVector[] pts = new PVector[fftLog.avgSize()];

  PVector[] trace = new PVector[0];

  Waveform(float incomingX, float incomingY, float incomingZ, float incomingRadius){
    x = incomingX;
    y = incomingY;
    z = incomingZ;
    radius = incomingRadius;
  }

  void update(){
    plot();
  }

  void plot(){
    for(int i = 0; i < fftLog.avgSize(); i++){
      int w = int(width/fftLog.avgSize());

      x = i*w; /// why "w"
      y = frameCount*50;/// regulation of density
      z = height/4-fftLog.getAvg(i)*5;/// regulation of hight

      stroke(0);
      point(x, y, z);
      pts[i] = new PVector(x, y, z);
      //increase size of array trace by length+1
      trace = (PVector[]) expand(trace, trace.length+1);
      //always get the next to last
      trace[trace.length-1] = new PVector(pts[i].y, pts[i].x, pts[i].z);
    }
  }


  void textdraw(){
    for(int i =0; i<fftLog.avgSize(); i++){    
      pushMatrix();
      translate(pts[i].x, pts[i].y, pts[i].z);
      rotateY(PI/2);
      rotateZ(PI/2);
      textFont(font);
      fill(255,200);
      text(round(fftLog.getAvg(i)*10),0,0,0);
      popMatrix();
    }
  }

  void plotTrace(){
    /*
    //drawing points
     for(int i=0; i<trace.length; i++){
     stroke(255,0,0);
     //locator(trace[i].x, trace[i].y, trace[i].z, 1);
     point(trace[i].x, trace[i].y, trace[i].z);
     }
    */ 
    //drawing poly surface


    stroke(255,80);
    int inc = fftLog.avgSize();

    for(int i=1; i<trace.length-inc; i++){
      if(i%inc != 0){
        beginShape(TRIANGLE_STRIP);
        float value = (trace[i].z*100); 
        float m = map(value, -500, 20000, 0, 255);
        fill(m*2, 125, -m*20, 140);
        vertex(trace[i].x, trace[i].y, trace[i].z);
        vertex(trace[i-1].x, trace[i-1].y, trace[i-1].z);
        vertex(trace[i+inc].x, trace[i+inc].y, trace[i+inc].z);
        vertex(trace[i-1+inc].x, trace[i-1+inc].y, trace[i-1+inc].z);
        endShape(CLOSE);


      }
    }
    /*
    //draw splines
     int skip = 5;
     stroke(255, 20);  
     noFill();
     for(int i=0; i<trace.length; i+=inc*skip){
     beginShape();
     
     for(int j=0; j<inc; j+=skip){
     stroke(255,0,0,40);
     curveVertex(trace[i+j].x, trace[i+j].y, trace[i+j].z);
     }
     endShape();
     }
     */  }
}


 
void keyPressed() {
  // use a key press so that it doesn't make a million files
  if (key == 's') record = true;
}