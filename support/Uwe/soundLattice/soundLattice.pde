//just a mix of 'DynamicMesh' from HE_Mesh library and "Frequency Energy Beat Detection" from Minim

import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;

import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer song;
BeatDetect beat;
BeatListener bl;

float kickSize, snareSize, hatSize;

class BeatListener implements AudioListener
{
  private BeatDetect beat;
  private AudioPlayer source;

  BeatListener(BeatDetect beat, AudioPlayer source)
  {
    this.source = source;
    this.source.addListener(this);
    this.beat = beat;
  }

  void samples(float[] samps)
  {
    beat.detect(source.mix);
  }

  void samples(float[] sampsL, float[] sampsR)
  {
    beat.detect(source.mix);
  }
}


HE_DynamicMesh dynMesh;
WB_Render render;
HEM_Lattice lattice;

PeasyCam cam;

void setup() {
  size(800, 800, P3D);
  cam=new PeasyCam(this, 800);

  minim = new Minim(this);  
  song = minim.loadFile("marcus_kellis_theme.mp3", 1024);
  song.play();
  beat = new BeatDetect(song.bufferSize(), song.sampleRate());
  beat.setSensitivity(300);  
  kickSize = snareSize = hatSize = 16;
  bl = new BeatListener(beat, song);  


  smooth(8);
  HE_Mesh cube=new HE_Mesh(new HEC_Cube().setEdge(600));  
  //a dynamic mesh is called with the base mesh as argument
  dynMesh = new HE_DynamicMesh(cube);

  //subdividors can be added directly as parameter, to be applied more than once it should be added again
  dynMesh.add(new HES_CatmullClark());

  //modifiers can also be added directly as parameter
  dynMesh.add(new HEM_Extrude().setDistance(0).setChamfer(0.5));
  //However adding implicitely is not useful as the parameters can no longer be changed.
  //It is better to apply these kind of fixed modifiers to the base mesh before passing it through to
  //the HE_DynamicMesh. This way their overhead is avoided each update().

  //Modifiers or subdividors that are to be dynamic should be called as referenced objects.
  lattice=new HEM_Lattice().setWidth(10).setDepth(5);
  dynMesh.add(lattice);
  //All modifiers and subdividors are applied on a call to update()
  dynMesh.update();

  render=new WB_Render(this);
}

void draw() {
  background(255);


  if ( beat.isKick() ) kickSize = 32;
  if ( beat.isSnare() ) snareSize = 32;
  if ( beat.isHat() ) hatSize = 32;  


  kickSize = constrain(kickSize * 0.95, 16, 32);
  snareSize = constrain(snareSize * 0.95, 16, 32);
  hatSize = constrain(hatSize * 0.95, 16, 32);


  lights();
  float d=-50.0+kickSize;
  float w=1.0+hatSize;
  lattice.setWidth(w).setDepth(d);
  dynMesh.update();
  fill(255);
  render.drawFaces(dynMesh);

}