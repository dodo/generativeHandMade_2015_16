import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

import toxi.geom.*;


PeasyCam cam;

boolean showLine=true;
boolean showSpline=true;
boolean showHandles=true;

int cLayer=0;
int stackDepth=100;

ArrayList<MySpline> allSplines= new ArrayList<MySpline>();

boolean showAll=false;

void setup() {
  size(1900, 1000, P3D);
  cam=new PeasyCam(this, 500);
  cam.setActive(false);
  smooth(32);

  for (int i=0; i<stackDepth; i++)allSplines.add(new MySpline(i));
}

void draw() {
  background(255);
  if (!showAll) {
    MySpline currentSpline = allSplines.get(cLayer);
    currentSpline.display();
  }
  if (showAll) {
    for (MySpline currentSpline : allSplines) {
      currentSpline.display();
    }
  }
}

void mouseDragged() {
  if (!showAll) {
    MySpline currentSpline = allSplines.get(cLayer);
    int numP=currentSpline.points.size();
    Vec3D currP=new Vec3D(map(mouseX,0,width,-300,300),map(mouseY,0,height,-300,300), map(cLayer, 0, stackDepth, -300, 300));
    if (numP>0) {
      // check distance to previous point
      Vec3D prevP=(Vec3D)currentSpline.points.get(numP-1);
      if (currP.distanceTo(prevP)>currentSpline.sampleDistance) {
        currentSpline.points.add(currP);
      }
    } else {
      // add first point regardless
      currentSpline.points.add(currP);
    }
  }
}


void keyPressed() {
  MySpline currentSpline = allSplines.get(cLayer);
  if (key=='h') showHandles=!showHandles;
  else if (key=='l') showLine=!showLine;
  else if (key=='s') showSpline=!showSpline;
  else if (key==' ')currentSpline.points.clear();
  else if (key=='q') {
    if (cLayer<stackDepth-1) {
      cLayer++;
    }
  } else if (key=='a') {
    if (cLayer>0) {
      cLayer--;
    }
  }
  if (key=='z') {
    if (showAll) {
      cam.setActive(false);
      showAll=false;
    } else {
      cam.setActive(true);
      showAll=true;
    }
  }
}