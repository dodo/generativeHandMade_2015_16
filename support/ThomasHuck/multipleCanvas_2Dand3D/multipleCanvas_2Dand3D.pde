import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

// Based on code by GeneKao (https://github.com/GeneKao)

ChildApplet child;
PeasyCam p_cam, c_cam; 

boolean mousePressedOnParent = false;

void settings() {
  size(900, 300);
  smooth(32);
}

void setup() {
  surface.setTitle("2D input surface");
  child = new ChildApplet();
}

void draw() {
  background(255);
  fill(100);
  ellipse(width/2,height/2,100,100);


  if (mousePressed) {
    mousePressedOnParent = true;
  }  
  if (child.mousePressed) {
    println("Mouse pressed on child.");
  }
}