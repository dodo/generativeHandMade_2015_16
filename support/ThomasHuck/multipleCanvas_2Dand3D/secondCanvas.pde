class ChildApplet extends PApplet {

  public ChildApplet() {
    super();
    PApplet.runSketch(new String[]{this.getClass().getName()}, this);
  }

  public void settings() {
    size(500, 500, P3D);
    smooth(32);
  }
  public void setup() { 
    surface.setTitle("3D preview");
    c_cam = new PeasyCam(this, 300);
  }

  public void draw() {
    background(0);
    fill(255);
    box(100, 200, 100);


    if (mousePressed) {
      mousePressedOnParent=false;
      println("Mouse pressed on child.");
    }
    if (mousePressedOnParent) {
      println("Mouse pressed on parent");
    }
  }
}