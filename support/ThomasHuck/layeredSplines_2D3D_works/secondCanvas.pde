class ChildApplet extends PApplet {



  public ChildApplet() {
    super();
    PApplet.runSketch(new String[]{this.getClass().getName()}, this);
  }

  public void settings() {
    size(500, 500, P3D);
    smooth(8);
  }
  public void setup() { 
    surface.setTitle("3D preview");
    c_cam = new PeasyCam(this, 100);
  }

  void draw() {
    background(255);
    strokeWeight(3);
    pushMatrix();
    translate(-width/2,-height/2,0);
    for (int i=0; i<pts.size(); i++) {
      Vec3D p=pts.get(i);
      point(p.x, p.y, p.z);
    }
    popMatrix();
  }
}