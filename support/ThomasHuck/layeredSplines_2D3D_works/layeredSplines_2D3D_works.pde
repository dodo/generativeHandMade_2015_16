import toxi.geom.*;

import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

ChildApplet child;
PeasyCam p_cam, c_cam; 
PImage img;

boolean mousePressedOnParent = false;

boolean showLine=true;
boolean showSpline=true;
boolean showHandles=true;

int cLayer=0;
int stackDepth=100;

ArrayList<MySpline> allSplines= new ArrayList<MySpline>();

ArrayList<Vec3D> pts=new ArrayList<Vec3D>();

PFont f;

boolean showAll=false;





void settings() {
  size(688, 684, P2D);
  smooth(32);
  }

void setup() {
  f = createFont("AnonymousPro-12.vlw", 12);
  textFont(f);
  surface.setTitle("2D input surface");
  child = new ChildApplet();

  for (int i=0; i<stackDepth; i++)allSplines.add(new MySpline(i));
  
}

void draw() {
  background(255);
  if (!showAll) {
    MySpline currentSpline = allSplines.get(cLayer);
    currentSpline.display2D();
  }
  if (showAll) {
    for (MySpline currentSpline : allSplines) {
      currentSpline.display2D();
    }
  }
  
  showReference();
}

void mouseDragged() {
  if (!showAll) {
    MySpline currentSpline = allSplines.get(cLayer);
    int numP=currentSpline.points.size();
    Vec3D currP=new Vec3D(mouseX, mouseY, cLayer);
    if (numP>0) {
      // check distance to previous point
      Vec3D prevP=(Vec3D)currentSpline.points.get(numP-1);
      if (currP.distanceTo(prevP)>currentSpline.sampleDistance) {
        currentSpline.points.add(currP);
        pts.add(currP);
      }
    } else {
      // add first point regardless
      currentSpline.points.add(currP);
      pts.add(currP);
    }
  }
}

void showReference() {
  fill(0);
  if (!showAll)text("current Layer: "+cLayer, 30, height-30);
  if (showAll)text("whole stack: "+stackDepth, 30, height-30);
}

void keyPressed() {
  MySpline currentSpline = allSplines.get(cLayer);
  if (key=='h') showHandles=!showHandles;
  else if (key=='l') showLine=!showLine;
  else if (key=='s') showSpline=!showSpline;
  else if (key==' ')currentSpline.points.clear();
  else if (key=='q') {
    if (cLayer<stackDepth-1) {
      cLayer++;
    }
  } else if (key=='a') {
    if (cLayer>0) {
      cLayer--;
    }
  }
  if (key=='z')showAll=!showAll;
}