class MySpline {
  ArrayList points=new ArrayList();

  // desired distance between points/handles
  int sampleDistance=10;
  
  int layer;

MySpline(int _layer){
layer=_layer;

}


  void display() {
    noFill();
    int numP=points.size();
    if (showLine) {
      stroke(255, 0, 0, 50);
      beginShape();
      for (int i=0; i<numP; i++) {
        Vec3D p=(Vec3D)points.get(i);
        vertex(p.x, p.y, p.z);
      }
      endShape();
    }
    stroke(0);
    // highlight the positions of the points with circles
    Vec3D[] handles=new Vec3D[numP];
    for (int i=0; i<numP; i++) {
      Vec3D p=(Vec3D)points.get(i);
      handles[i]=p;
      if (showHandles) ellipse(p.x, p.y, 5, 5);
    }

    // need at least 4 vertices for a spline
    if (numP>3 && showSpline) {
      // pass the points into the Spline container class
      Spline3D spline=new Spline3D(handles);
      // sample the curve at a higher resolution
      // so that we get extra 8 points between each original pair of points
      java.util.List vertices=spline.computeVertices(8);
      // draw the smoothened curve
      beginShape();
      for (int i=0; i<vertices.size(); i++) {
        Vec3D v=(Vec3D)vertices.get(i);
        vertex(v.x, v.y, v.z);
      }
      endShape();
    }
  }
}