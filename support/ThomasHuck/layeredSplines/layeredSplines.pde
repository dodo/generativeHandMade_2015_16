import toxi.geom.*;

boolean showLine=true;
boolean showSpline=true;
boolean showHandles=true;

int cLayer=0;
int stackDepth=100;

ArrayList<MySpline> allSplines= new ArrayList<MySpline>();

PFont f;

boolean showAll=false;

void setup() {
  size(1900, 1000, P3D);
  smooth(32);
  f = createFont("AnonymousPro-12.vlw", 12);
  textFont(f);


  for (int i=0; i<stackDepth; i++)allSplines.add(new MySpline(i));
}

void draw() {
  background(255);
  if (!showAll) {
    MySpline currentSpline = allSplines.get(cLayer);
    currentSpline.display();
  }
  if (showAll) {
    for (MySpline currentSpline : allSplines) {
      currentSpline.display();
    }
  }
  showReference();
}

void mouseDragged() {
  if (!showAll) {
    MySpline currentSpline = allSplines.get(cLayer);
    int numP=currentSpline.points.size();
    Vec3D currP=new Vec3D(mouseX, mouseY, cLayer);
    if (numP>0) {
      // check distance to previous point
      Vec3D prevP=(Vec3D)currentSpline.points.get(numP-1);
      if (currP.distanceTo(prevP)>currentSpline.sampleDistance) {
        currentSpline.points.add(currP);
      }
    } else {
      // add first point regardless
      currentSpline.points.add(currP);
    }
  }
}

void showReference() {
  fill(0);
  if (!showAll)text("current Layer: "+cLayer, 30, height-30);
  if (showAll)text("whole stack: "+stackDepth, 30, height-30);
}

void keyPressed() {
  MySpline currentSpline = allSplines.get(cLayer);
  if (key=='h') showHandles=!showHandles;
  else if (key=='l') showLine=!showLine;
  else if (key=='s') showSpline=!showSpline;
  else if (key==' ')currentSpline.points.clear();
  else if (key=='q') {
    if (cLayer<stackDepth-1) {
      cLayer++;
    }
  } else if (key=='a') {
    if (cLayer>0) {
      cLayer--;
    }
  }
  if (key=='z')showAll=!showAll;
}