/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/10984*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
// drawing + motion + 3D + curve
// Memory Mouse Data, Using Class + 3D.

//import processing.opengl.*;

drawMotion3D dm3d;
PImage img;
import peasy.*;
PeasyCam cam;


void setup() {
  img=loadImage("akt.JPG");
  background(img);
  size(1100, 619, P3D);
  dm3d = new drawMotion3D(100, 8.0, 0.75);
  // numbers of line point, change values, depth of z-positions
  noFill();
  cam = new PeasyCam(this, 100);
  cam.setMinimumDistance(50);
  cam.setMaximumDistance(500);

//  textSize(12);
}

void draw() {
  background(200);
  noFill();
  box(100,100,100);

  dm3d.display();

//  text("mouse click & drag = draw line\nmouse move = rotate in 3D\n'm' = change to straight/curve\n'SPACE' = re-positioning", 5, 17);

}



void mouseDragged() {
  dm3d.keep();
}



void keyPressed(){
  if(key=='s')saveFrame("pict-####.jpg");
  if(key==' ') dm3d.init();
  if(key=='q'){
   if(dm3d.on == true) dm3d.off();
   else{
    dm3d.on = true;
   }
  }
}