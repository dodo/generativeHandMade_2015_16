class MyShape {
  HE_Mesh mesh;
  HE_Selection selection;
  HE_Selection nuThings;
  ArrayList<MyPoint> pts=new ArrayList<MyPoint>();
  long cKey;

  MyShape() {
    HEC_FromBinarySTLFile loader=new HEC_FromBinarySTLFile (sketchPath()+"/data/vaults.stl");// all in one line: declare a loader and load from the file at specified location, try your own mesh!
    mesh=new HE_Mesh(loader);//create a new mesh from what we just loaded
    mesh.scale(20);//obvious, right?
    selection=new HE_Selection(mesh);
    makeSpringy();
  }


  void update() {//updates the mesh vertices, so that they match particles positions (pts arraylist stores all the particles related to the mesh vertices)
    for (MyPoint p : pts) {
      HE_Vertex v = mesh.getVertexByKey(p.myKey);
      v.set(p.x, p.y, p.z);
    }
  }


  void display() {
    stroke(0);
    strokeWeight(0.5);
    fill(255);
    render.drawFaces(mesh);//display the mesh on the screen, taken from hemesh library examples
    stroke(255, 0, 0);
    noFill();
    if (mode) {
      selrender.drawVertices(mesh, 16);
      cKey=selrender.getKey();
      render.drawVertex(cKey, 16, mesh);
    } else {
      selrender.drawFaces(mesh);
      cKey=selrender.getKey();
      render.drawFace(cKey, mesh);
    }
    for(MyPoint p:pts)p.display();
  }

  void makeSpringy() {
    HE_VertexIterator vItr=new HE_VertexIterator(mesh);
    while (vItr.hasNext ()) {
      HE_Vertex v=vItr.next();
      pts.add(new MyPoint(new Vec3D(v.xf(), v.yf(), v.zf()), v.getKey()));
    }

    HE_EdgeIterator eItr=new HE_EdgeIterator(mesh);
    while (eItr.hasNext ()) {
      HE_Halfedge he=eItr.next();
      long svk=he.getStartVertex().getKey();
      long fvk=he.getEndVertex().getKey();
      for (MyPoint p : pts) {
        if (p.myKey==svk) {
          for (MyPoint pp : pts) {
            if (pp.myKey==fvk) {
              VerletSpring cs=new VerletSpring(p, pp, (float)he.getLength(), 0.001);
              p.mySprings.add(cs);
              pp.mySprings.add(cs);
              world.addSpring(cs);
            }
          }
        }
      }
    }
  }
}