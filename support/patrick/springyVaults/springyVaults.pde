import controlP5.*;
import wblut.core.*;
import wblut.geom.*;
import wblut.hemesh.*;
import wblut.math.*;
import wblut.processing.*;
import toxi.physics.*;//requires toxiclibs library: http://toxiclibs.org/
import toxi.physics.behaviors.*;
import toxi.physics.constraints.*;
import toxi.math.conversion.*;
import toxi.geom.*;
import toxi.math.*;
import toxi.util.datatypes.*;
import toxi.util.events.*;
import toxi.util.*;
import peasy.*;//import peasyCam library
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;


PeasyCam cam;//declare 'cam' as a PeasyCam instance
ControlP5 gui;
//declare mesh as an instance of HE_Mesh
WB_Render render;//same..
WB_SelectRender3D selrender;
VerletPhysics world;

MyShape s;

boolean update=false;
boolean mode = true;

void setup() {
  size(1000, 1000, P3D); 
  cam=new PeasyCam(this, 700);//create a new camera object for 'this' sketch, 500 away from cartesian 0 of the scene
  gui=new ControlP5(this);
  world=new VerletPhysics();
  world.setDrag(0.01);  
  render=new WB_Render(this);//create a new rendering object for 'this' sketch
  selrender=new WB_SelectRender3D(this);
  s=new MyShape();  
  initGui();  
  smooth(32);
}

void draw() {
  if (update)world.update();
  background(255);
  lights();
  s.update();
  s.display();
  noLights();
  showGui();
}