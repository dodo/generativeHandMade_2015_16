//inspired by Nagual [Rob Saunders], just very simple...
//echoechonoisenoise
 
ArrayList p;
 
void setup(){
  size(1000,1000);
  background(255);
  stroke(150);
  smooth(8);
  p=new ArrayList();
  
  initFromEdges(1);//start growing from edges, number indicates type of growth
  
}
 
void draw(){
  for(int i=0;i<p.size();i++){
    Particles c=(Particles) p.get(i);
    c.hit();
    c.display();
    c.move();
    c.giveBirth();
  }
  cleanup();
  
  //saveFrame("animation/frame-####.tga");// uncomment this to record animation frames
  
}
 
void mouseDragged(){
  for(int i=0;i<30;i++){
    if(mouseButton==LEFT)p.add(new Particles(new PVector(mouseX,mouseY),new PVector(random(-1,1),random(-1,1)),1));//last number (0,1 or 2) indicates the type of growth
    if(mouseButton==RIGHT)p.add(new Particles(new PVector(mouseX,mouseY),new PVector(random(-1,1),random(-1,1)),0));
    if(mouseButton==CENTER)p.add(new Particles(new PVector(mouseX,mouseY),new PVector(random(-1,1),random(-1,1)),2)); 
  }
}
 
 void initFromEdges(int type){
   for(int i=0;i<100;i++)p.add(new Particles(new PVector(random(width),0),new PVector(random(-1,1),random(-1,1)),type));
   for(int i=0;i<100;i++)p.add(new Particles(new PVector(random(width),height-1),new PVector(random(-1,1),random(-1,1)),type));
   for(int i=0;i<100;i++)p.add(new Particles(new PVector(0,random(height)),new PVector(random(-1,1),random(-1,1)),type));
   for(int i=0;i<100;i++)p.add(new Particles(new PVector(width-1,random(height)),new PVector(random(-1,1),random(-1,1)),type));
 }
 
void cleanup(){
  for(int i=0;i<p.size();i++){
    Particles c=(Particles) p.get(i);
    if(c.running==false){
      p.remove(i);     
    }
  }
}