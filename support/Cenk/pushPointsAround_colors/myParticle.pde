class MyParticle extends VerletParticle {

  color c;

  MyParticle(Vec3D pos, color _c) {
    super(pos);
    c=_c;
  }
}