import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

import toxi.math.conversion.*;//requires toxiclibs library: http://toxiclibs.org/
import toxi.geom.*;
import toxi.math.*;
import toxi.util.datatypes.*;
import toxi.util.events.*;
import toxi.util.*;
import toxi.physics.*;
import toxi.physics.constraints.*;
import toxi.physics.behaviors.*;

PeasyCam cam;

VerletPhysics world;

ArrayList<PVector> allPoints = new ArrayList<PVector>();

ArrayList<MyParticle> dust = new ArrayList();
ArrayList cars = new ArrayList();

int carsAmount =10;
float speed =1;
int spread=50;
int attrDist=70;
int repDist=30;

String[] filename ={ "F_F_Gf_001.xyz","F_F_Gf_002.xyz","F_F_Gf_003.xyz" };//anything that is a plain text will do
int skipFront=0;//how long the header is?
int skipBack=1;//...
char spacer = ' ';//what separates coordinate data in your file?

float scl=30;//scaling all the points

PImage sprite;//displaying points as textured quads is waaay faster than using the point() function! 
float pointSize = 2;

int redu=7;//reduction (0,10), bigger number = less particles

void setup() {
  size(1900, 1000, P3D);
  cam=new PeasyCam(this, 500);
  sprite = loadImage("sprite.png");

  hint(DISABLE_DEPTH_MASK);

  world=new VerletPhysics();
  world.setDrag(0.01);
  for (int i=0;i<3;i++)loadData(i);
  for (int i=0; i<carsAmount; i++) {
    VerletParticle c=new VerletParticle(random(-spread, spread), random(-spread, spread), random(-spread, spread));
    c.addVelocity(new Vec3D(random(-speed, speed), random(-speed, speed), random(-speed, speed)));
    world.addBehavior(new AttractionBehavior(c, repDist, -0.03));
    world.addBehavior(new AttractionBehavior(c, attrDist, 0.001));
    world.addParticle(c);
    cars.add(c);
  }
}


void draw() {
  background(255);
  world.update();

  for (MyParticle pt : dust)drawPoint(pt, pt.c);

  stroke(200, 0, 100);
  strokeWeight(10);
  for (int i=0; i<cars.size(); i++) {
    VerletParticle c=(VerletParticle)cars.get(i);
    if (c.getVelocity().magnitude()<0.05)c.addVelocity(new Vec3D(random(-1, 1), random(-1, 1), random(-0.1, 0.1)));
    point(c.x, c.y, c.z);
  }
}

void drawPoint(Vec3D coords, color c) {
  // point(coords.x,coords.y,coords.z);
  beginShape(QUAD);
  noStroke();
  tint(c);
  texture(sprite);
  vertex(coords.x - pointSize/2, coords.y - pointSize/2, coords.z, 0, 0);
  vertex(coords.x + pointSize/2, coords.y - pointSize/2, coords.z, sprite.width, 0);
  vertex(coords.x + pointSize/2, coords.y + pointSize/2, coords.z, sprite.width, sprite.height);
  vertex(coords.x - pointSize/2, coords.y + pointSize/2, coords.z, 0, sprite.height);                
  endShape();
}


void loadData(int fileNumber) {
  String[] lines=loadStrings(filename[fileNumber]);//array storing all the lines loaded from the file
  String[][] chunks=new String[lines.length][];//array storing each line splitted into pieces 

  //split each line into chunks divided by whatever the spacer is
  for (int i=0; i<lines.length; i++) {
    chunks[i]=split(lines[i], spacer);
  }

  for (int i=skipFront; i<lines.length-skipBack; i++) {//skip the header and the footer of the file and..
    float  cx=Float.parseFloat(chunks[i][0]);//parse the values into floats
    float  cy=Float.parseFloat(chunks[i][2]);
    float  cz=Float.parseFloat(chunks[i][1]);
    float  cR=Float.parseFloat(chunks[i][3]);
    float  cG=Float.parseFloat(chunks[i][4]);
    float  cB=Float.parseFloat(chunks[i][5]);
    // allPoints.add(new PVector(cx, cy, cz).mult(scl));//create vectors out of coordinates, scale and store them in arraylist
    if(random(10)>redu){//skip some points..
    MyParticle p=new MyParticle(new Vec3D(cx, cy*(-1), cz).addSelf(new Vec3D(27, 55, 55)).scaleSelf(scl),color(cR,cG,cB));
    world.addParticle(p);
    dust.add(p);
    }
  }

  println("gloriously loaded "+dust.size()+" points");
} 