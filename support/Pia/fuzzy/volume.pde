void makeVolume() {
  vol=new VolumetricSpaceArray(scl, dim, dim, dim);
  brush=new RoundBrush(vol, 1);
  m=new WETriangleMesh();
  InterpolateStrategy linear=new LinearInterpolation();//this is part of toxiclibscore library, it just helps finding values between given ones... check interpolate example 
brush.setSize(1);
  //here is the tricky bit:
  for (int i=0;i<dots.size();i++) {//for all the elements of "dots" list
    Dot d=(Dot)dots.get(i);//pick one
  for(int j=1;j<d.path.size();j++){//for the whole length of the path
   Vec3D a=d.path.get(j-1); 
   Vec3D b=d.path.get(j); 
    Line3D l = new Line3D(a, b);//draw a line
    ArrayList<Vec3D> pts=new ArrayList<Vec3D>();
    l.splitIntoSegments(pts, 1, false);//and split it
    for (int k=0;k<pts.size();k++) {//for each split point along the line
      Vec3D p=pts.get(k);
     brush.drawAtAbsolutePos(p, 1);//and draw along the line
    }
  }
  }
  vol.closeSides();
  srf=new ArrayIsoSurface(vol);
  srf.computeSurfaceMesh(m, thrs);
  showMesh=true;
}