//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//about sketching in a field and wraping a sketch in volume

import toxi.processing.*;
import toxi.volume.*;
import toxi.math.conversion.*;
import toxi.geom.*;
import toxi.math.*;
import toxi.geom.mesh2d.*;
import toxi.util.datatypes.*;
import toxi.util.events.*;
import toxi.geom.mesh.subdiv.*;
import toxi.geom.mesh.*;
import toxi.math.waves.*;
import toxi.util.*;
import toxi.math.noise.*;
import toxi.physics.*;
import toxi.physics.constraints.*;
import toxi.physics.behaviors.*;
import remixlab.bias.event.*;
import remixlab.bias.event.shortcut.*;
import remixlab.bias.agent.profile.*;
import remixlab.bias.agent.*;
import remixlab.dandelion.agent.*;
import remixlab.proscene.*;
import remixlab.dandelion.core.*;
import remixlab.dandelion.constraint.*;
import remixlab.fpstiming.*;
import remixlab.util.*;
import remixlab.dandelion.geom.*;
import remixlab.bias.core.*;

import wblut.core.*;//import wblut [hemesh 2014] library
import wblut.geom.*;
import wblut.hemesh.*;
import wblut.math.*;
import wblut.processing.*;


Scene scn;//define "scn"as Scene(part of proscene library)
ArrayList dots = new ArrayList();//create an empty list to store the particles (dots in this case, just the name..)
float nsc=0.05;//noise scale, check processing help noise() function

//everything below referes to the volume, check previous tutorials and volumeutils examples
int dim =400;
int sc=400;
Vec3D scl = new Vec3D(sc, sc, sc);
float thrs=0.5;
VolumetricSpaceArray vol;
IsoSurface srf;
WETriangleMesh m;
VolumetricBrush brush;
/////////////////////
ToxiclibsSupport gfx;

boolean showMesh=false;

HE_Mesh mesh;//declare mesh as an instance of HE_Mesh
WB_Render render;//same..

void setup() {
  size(1900, 1000, P3D);  
  scn=new Scene(this);//create the scene
  //scn.setAxisVisualHint(false);//disable display of axes in the scene, refere to proscene html document, or to the examples that go with it
  scn.setRadius(1000);

  render=new WB_Render(this);//create a new rendering object for 'this' sketch

  initMeshCanvas();//check 'turnIntoDots' tab

  for (int i=0; i<10; i++)dots.add(new Dot(new Vec3D(random(-10, 10), random(-10, 10), random(-10, 10)), false));//create 10 new dots in a random place and add them to "dots" list
  makeVolume();//check 'volume' tab
  gfx=new ToxiclibsSupport(this);
  smooth(32);//turn on anti-aliasing, check processing help
}


void draw() {
  background(255); 

  strokeWeight(0.5);
  lights();
  if (showMesh)gfx.mesh(m);//display the meshed structure
  render.drawFaces(mesh);//display the loaded mesh
  
    for (int i=0; i<dots.size(); i++) {//for all the elements of "dots" list
    Dot d=(Dot)dots.get(i);//pick one
    d.update();//call its internal "update" function
    d.display();//call its internal "display" function
  }
}


void keyPressed() {
  if (key=='q'&&nsc<0.5)nsc+=0.001;
  if (key=='w'&&nsc>0.0001)nsc-=0.001;
  if (key=='v') {
    makeVolume();
  }
  if (key == 'l') {
    new LaplacianSmooth().filter(m, 1);
  }
  if (key == 's') {//save it as stl
    m.saveAsSTL(sketchPath("volume_"+month()+day()+hour()+minute()+".stl"));
  }
  if (key=='m')showMesh=!showMesh;
  println(nsc);
}