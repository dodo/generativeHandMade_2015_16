void initMeshCanvas() {
  HEC_FromBinarySTLFile loader=new HEC_FromBinarySTLFile (sketchPath()+"/data/body.stl");// all in one line: declare a loader and load from the file at specified location
  mesh=new HE_Mesh(loader);//create a new mesh from what we just loaded
  mesh.scale(30);//obvious, right?

  HE_VertexIterator vItr=new HE_VertexIterator(mesh);
  while (vItr.hasNext ()) {
    HE_Vertex v=vItr.next();

    dots.add(new Dot(new Vec3D(v.xf(), v.yf(), v.zf()), true));//add dots for each vertex of the mesh
  }
}