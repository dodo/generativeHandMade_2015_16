PGraphics g;

void setup() {
  size(1000, 1000);  
  g=createGraphics(1000, 1000);
}


void draw() {
  g.beginDraw();
  g.ellipse(500, 500, 100, 100);
  g.endDraw();
  image(g, 0, 0);
}

void keyPressed() {
  g.save("pic.png");
}