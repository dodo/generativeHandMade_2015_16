class Line_x {
  PVector bg;  
  PVector fn; 

  Line_x(PVector _bg, PVector _fn) {
    bg=_bg;
    fn=_fn;
  }
  
  void display(){
    line(bg.x,bg.y,bg.z,fn.x,fn.y,fn.z);
  }
}