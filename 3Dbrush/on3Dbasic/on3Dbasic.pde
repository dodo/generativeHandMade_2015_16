import wblut.core.*;//import wblut [hemesh 2014] library
import wblut.geom.*;
import wblut.hemesh.*;
import wblut.math.*;
import wblut.processing.*;

import peasy.*;//import peasyCam library
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;


PeasyCam cam;//declare 'cam' as a PeasyCam instance

HE_Mesh mesh;//declare mesh as an instance of HE_Mesh
WB_Render render;//same..

void setup() {
  size(1000, 1000, P3D); 
  cam=new PeasyCam(this, 500);//create a new camera object for 'this' sketch, 500 away from cartesian 0 of the scene
  render=new WB_Render(this);//create a new rendering object for 'this' sketch
  HEC_FromBinarySTLFile loader=new HEC_FromBinarySTLFile (sketchPath()+"/data/chicken.stl");// all in one line: declare a loader and load from the file at specified location
  mesh=new HE_Mesh(loader);//create a new mesh from what we just loaded
  mesh.scale(200);//obvious, right?

//just copied and pasted from one of 'modifiers' examples from hemesh library:
  HEM_Lattice modifier=new HEM_Lattice();
  modifier.setWidth(3);// desired width of struts
  modifier.setDepth(3);// depth of struts
  modifier.setThresholdAngle(1.5*HALF_PI);// treat edges sharper than this angle as hard edges
  modifier.setFuse(true);// try to fuse planar adjacent planar faces created by the extrude
  modifier.setFuseAngle(0.1*HALF_PI);// threshold angle to be considered coplanar
  mesh.modify(modifier);
  mesh.validate();
  mesh.smooth();

  smooth(8);
}

void draw() {
  background(255);
  lights();
  stroke(0);
  strokeWeight(0.5);
  
  render.drawFaces(mesh);//display the mesh on the screen, taken from hemesh library examples
}