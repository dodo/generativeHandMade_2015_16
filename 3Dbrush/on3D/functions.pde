void loadMesh() {// load the meshes
  HEC_FromBinarySTLFile creator;

  creator=new HEC_FromBinarySTLFile(sketchPath()+"/data/chicken.stl");
  mesh1=new HE_Mesh(creator);
  mesh1.scale(100);//and scale them

  creator=new HEC_FromBinarySTLFile(sketchPath()+"/data/monkey_s.stl");
  mesh2=new HE_Mesh(creator);
  mesh2.scale(30);
}



void brushStroke(PVector _initPos, int brushNo) {//brushStroke requires a position [in that case it is a position of a vertex under the mouse ]  and the brush type.

  PVector pos=_initPos;
  PVector myAxis=new PVector(pos.x+random(-1, 1), pos.y+random(-1, 1), pos.z+random(-1, 1));//define a random axis related to our current position, for some fun below
  HE_Mesh myStroke;  
  
  switch(brushNo) {//check switch function in processing reference
  case 1:
    myStroke=new HE_Mesh(mesh1);
    break;
  case 2:
    myStroke=new HE_Mesh(mesh2);
    break;
  default:
    myStroke=new HE_Mesh(mesh1);
    break;
  }
  myStroke.moveTo(pos.x, pos.y, pos.z);
  myStroke.rotateAboutAxis(45, pos.x, pos.y, pos.z, myAxis.x, myAxis.y, myAxis.z);
  myStroke.scale(random(0.8, 2));
  // myStroke.modify(new HEM_PunchHoles().setWidth(3));
  //myStroke.modify(new HEM_VertexExpand().setDistance(random(0,10)));
  myStroke.modify( new HEM_Twist().setTwistAxis(new WB_Point( pos.x, pos.y, pos.z), new WB_Point( myAxis.x, myAxis.y, myAxis.z)).setAngleFactor(random(0, 1)));

  fused.add(myStroke);//add current stroke to the 'fused' mesh
}