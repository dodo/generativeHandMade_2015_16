import wblut.core.*;
import wblut.geom.*;
import wblut.hemesh.*;
import wblut.math.*;
import wblut.processing.*;

import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

PeasyCam cam;

HE_Mesh mesh1, mesh2;//declare the names for meshes loaded, check 'functions' tab to understand where do they come from
HE_Mesh fused;// a name for a resulting mesh
WB_Render render;
WB_SelectRender3D selrender;//this class allows selecting elements of the mesh under mouse cursor. check hemesh library examples ( selecting vertices, selecting faces etc )

ArrayList allStrokes = new ArrayList();

void setup() {
  size(1000, 1000, P3D);
  cam=new PeasyCam(this, 500);
  render=new WB_Render(this);
  selrender=new WB_SelectRender3D(this);

  loadMesh();//check what this function does below
  fused=new HE_Mesh(mesh1);


  smooth(8);
}

void draw() {
  background(255);
  fill(255);
  lights();
  stroke(0);
  strokeWeight(0.5);
  render.drawFaces(fused);//display faces of the mesh
  selrender.drawVertices(fused, 15);
  noFill();
  stroke(255, 0, 0);
  render.drawVertex(selrender.getKey(), 15, fused);//this will display a vertex under mouse cursor as a box 15x15x15
}



void keyPressed() {
  if (key=='q') {
    long currV=selrender.getKey();//get the name of the vertex currently under the mouse

    HE_Vertex v= fused.getVertexByKey(currV);//get a vertex with that name frome 'fused'mesh
    if (v!=null) {//only if there is such vertex, do the line below ( sometimes you might point somewhere in the space, where there are no valid vertices, always good to make sure
      brushStroke(new PVector(v.xf(), v.yf(), v.zf()), 1);//check 'brushStroke' function in 'functions' tab!
    }
  }
  if (key=='w') {//same as the function above
    long currV=selrender.getKey();

    HE_Vertex v= fused.getVertexByKey(currV);
    if (v!=null) {
      brushStroke(new PVector(v.xf(), v.yf(), v.zf()), 2);
    }
  }
  if (key=='s')fused.smooth();//smooth the mesh
  if (key=='l') {//make a lattice, taken from one of library examples..
    HEM_Lattice modifier=new HEM_Lattice();
    modifier.setWidth(2);// desired width of struts
    modifier.setDepth(1);// depth of struts
    modifier.setThresholdAngle(1.5*HALF_PI);// treat edges sharper than this angle as hard edges
    modifier.setFuse(true);// try to fuse planar adjacent planar faces created by the extrude
    modifier.setFuseAngle(0.1*HALF_PI);// threshold angle to be considered coplanar
    fused.modify(modifier);
    fused.validate();
  }
  if (key=='c')fused.modify(new HEM_Slice().setPlane(0, 0, -50, 0, 0, 1));//slice the mesh with a plane defined in parenthesis, from examples as well
  if (key=='x')HET_Export.saveToPLY(fused, sketchPath("meshes"), "test");//export as ply. sometimes generates weird results. will fix it soon, no worries
}