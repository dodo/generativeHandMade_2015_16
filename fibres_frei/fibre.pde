class Fibre {

  ArrayList<Dot> dots = new ArrayList<Dot>();

int subd=20;//how fine is the fibres subdivision.. smaller number results in more particles per fibre

  Fibre(Vec3D bg, Vec3D fin) {
    Line3D f=new Line3D(bg, fin);
    ArrayList<Vec3D> trc=new ArrayList<Vec3D>();
    f.splitIntoSegments(bg, fin, subd, trc, true);//subdivide the line into pieces 
    for (Vec3D v : trc) {
      dots.add(new Dot(v));//add a dot for each segment
    }
    for (int i=1; i<dots.size(); i++) {           
      Dot a=(Dot)dots.get(i-1);
      Dot b=(Dot)dots.get(i);
      if (i==1)a.lock();
      if (i==dots.size()-1)b.lock();
      physics.addSpring(new VerletSpring(a, b, subd, 0.03));//add a spring between each two dots in a chain
    }
  }

  void display() {
    strokeWeight(0.33);
    for (int i=1; i<dots.size(); i++) {
      Dot a=(Dot)dots.get(i-1);
      Dot b=(Dot)dots.get(i);
      line(a.x, a.y, a.z, b.x, b.y, b.z);
    }
    strokeWeight(3);
    stroke(0);  
    if (!record)for (Dot d : dots)d.display();//show all the dots belonging to this fibre
  }
}

class Dot extends VerletParticle {

  Dot(Vec3D _pos) {
    super(_pos);
    this.setWeight(0.4);
    physics.addBehavior(new AttractionBehavior(this, 20, 0.01f));//"cohesion" force
    physics.addParticle(this);
  }

  void display() {     
    point(this.x, this.y, this.z);
  }
}


void volumeWrap() {//wraps all the springs in volume
  brush=new RoundBrush(volume, SCALE.x/2);
  brush.setSize(1);
  for (VerletSpring cs : physics.springs) {
    Line3D cl = new Line3D(cs.a, cs.b);
    ArrayList<Vec3D> trc=new ArrayList<Vec3D>();
    cl.splitIntoSegments(cs.a, cs.b, 1, trc, true);//subdivide the line into pieces 
    for (Vec3D v : trc) {
      brush.drawAtAbsolutePos(v, 1);
    }
  }
  volume.closeSides();
  // store in IsoSurface and compute surface mesh for the given threshold value
  mesh=new WETriangleMesh("iso");
  surface=new HashIsoSurface(volume, 0.333333);
  surface.computeSurfaceMesh(mesh, 0.5);
}