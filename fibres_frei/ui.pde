void keyPressed() {
  if (key==' ')update=!update;
  if (key=='r')record=true;
  if (key=='g')physics.addBehavior(new GravityBehavior(new Vec3D(0, 0, -0.1)));
  if (key=='c') {
    for (Fibre f : fibres) {
      for (Dot d : f.dots) {
        for (Fibre df : fibres) {
          for (Dot dd : df.dots) {
            if (d.distanceTo(dd)<10) {
              VerletSpring cs=new VerletSpring(d, dd, 3, 0.1);
              connections.add(cs);
              physics.addSpring(cs);
            }
          }
        }
      }
    }
  }
  if (key=='v')volumeWrap();
  if (key=='s') {
    mesh.saveAsSTL(sketchPath("frei_####.stl"));
  }
  if (key=='l')    new LaplacianSmooth().filter(mesh, 1);
}