import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

import processing.dxf.*;

import toxi.geom.*;
import toxi.geom.mesh.*;
import toxi.processing.*;
import toxi.physics.*;
import toxi.physics.behaviors.*;
import toxi.volume.*;

VerletPhysics physics;
GravityBehavior g;

//volume resolution:
int DIMX=800;
int DIMY=800;
int DIMZ=800;

//volume scale:
Vec3D SCALE=new Vec3D(1, 1, 1).scaleSelf(800);
VolumetricSpace volume=new VolumetricSpaceArray(SCALE, DIMX, DIMY, DIMZ);//initialize an empty volume of certain scale and resolution

IsoSurface surface;
WETriangleMesh mesh=new WETriangleMesh("iso");
VolumetricBrush brush;

//rendering support
ToxiclibsSupport gfx;

ArrayList<Fibre> fibres = new ArrayList<Fibre>();

ArrayList<VerletSpring> connections = new ArrayList<VerletSpring>();

boolean update= true;
boolean record = false;

PeasyCam cam;

void setup() {
  size(1600, 800, P3D);
  cam=new PeasyCam(this, 800);
  gfx=new ToxiclibsSupport(this);

  physics=new VerletPhysics();
  //physics.addBehavior(new GravityBehavior(new Vec3D(0, 0, -0.01)));
  physics.setDrag(0.01);

  for (int i=0; i<100; i++) {
    float pa=random(TWO_PI);
    float pb=random(TWO_PI);
    fibres.add(new Fibre(new Vec3D(sin(pa)*400, cos(pa)*400, 0), new Vec3D(sin(pb)*400, cos(pb)*400, 0)));
  }
  smooth(32);
}

void draw() {
  if (update)physics.update();
  if (record == true) {
    beginRaw(DXF, "output.dxf"); // Start recording to the file
  }
  background(255);


  for (Fibre f : fibres)f.display();
  for (VerletSpring cc : connections)line(cc.a.x, cc.a.y, cc.a.z, cc.b.x, cc.b.y, cc.b.z);

  noStroke();
  lights();

  gfx.mesh(mesh);
  if (record == true) {
    endRaw();
    record = false; // Stop recording to the file
  }
}