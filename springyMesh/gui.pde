
void initGui() {
  gui.setAutoDraw(false);
  gui.addToggle("update")
    .setPosition(30, 30)
    .setSize(30, 30)
    .setColorCaptionLabel(200)
    .setColorActive(color(200, 0, 100))
    .setColorBackground(color(200))
    .setColorForeground(color(255, 0, 150))
    ;
  gui.addBang("addG")
    .setPosition(70, 30)
    .setSize(30, 30)
    .setTriggerEvent(Bang.RELEASE)
    .setColorCaptionLabel(200)
    .setColorActive(color(200, 0, 100))
    .setColorBackground(color(200))
    .setColorForeground(color(255, 0, 150))
    ;
  gui.addBang("revG")
    .setPosition(110, 30)
    .setSize(30, 30)
    .setTriggerEvent(Bang.RELEASE)
    .setColorCaptionLabel(200)
    .setColorActive(color(200, 0, 100))
    .setColorBackground(color(200))
    .setColorForeground(color(255, 0, 150))
    ;
  gui.addToggle("modeSwitch")
    .setPosition(150, 30)
    .setSize(70, 15)
    .setValue(true)
    .setMode(ControlP5.SWITCH)
    .setColorCaptionLabel(200)
    .setColorActive(color(255, 0, 150))
    .setColorBackground(color(200))
    .setColorForeground(color(200, 0, 100))
    .setLabel("vertex_or_face")
    ;
  gui.addSlider("stiffness")
    .setPosition(30, 90)
    .setSize(150, 15)
    .setRange(1, 200)
    .setValue(100)
    .setColorCaptionLabel(200)
    .setColorActive(color(255, 0, 150))
    .setColorBackground(color(200))
    .setColorForeground(color(200, 0, 100))
    ;
}

public void addG() {
  world.addBehavior(new GravityBehavior(new Vec3D(0, 0, 0.01)));
}
public void revG() {
  world.addBehavior(new GravityBehavior(new Vec3D(0, 0, -0.01)));
}

public void modeSwitch(boolean theFlag) {
  mode=theFlag;
}
public void stiffness(int theValue){
  for(VerletSpring s:world.springs){
    s.setStrength(theValue*0.0001);
  }
  
}





void showGui() {
  hint(DISABLE_DEPTH_TEST);
  cam.beginHUD();
  gui.draw();
  cam.endHUD();
  hint(ENABLE_DEPTH_TEST);
}


void keyPressed() {
  if (!mode) {
    if (key=='l') {//turns the whole mesh into a lattice, then turns all the resulting edges into springs. watch out, heavy.
      HEM_Lattice modifier=new HEM_Lattice();
      modifier.setWidth(5);// desired width of struts
      modifier.setDepth(5);// depth of struts
      modifier.setThresholdAngle(1.5*HALF_PI);// treat edges sharper than this angle as hard edges
      modifier.setFuse(true);// try to fuse planar adjacent planar faces created by the extrude
      modifier.setFuseAngle(0.1*HALF_PI);// threshold angle to be considered coplanar
      s.mesh.modify(modifier);
      s.mesh.validate();
      s.pts.clear();
      world.clear();
      s.makeSpringy();
    }
    if (key=='s'&&s.cKey>0) {// subdivides the selected face into 3, and adds it to the springy system

      HE_Face cf=s.mesh.getFaceByKey(s.cKey);
      s.selection.add(cf);
      HE_FaceVertexCirculator fvCrc=new HE_FaceVertexCirculator(cf);
      while (fvCrc.hasNext ()) {
        long vk=fvCrc.next().getKey();
        for (MyPoint p : s.pts) {
          if (p.myKey==vk) {
            p.strech();
          }
        }
      }
      s.nuThings=s.mesh.splitFacesTri(s.selection);
      HE_Vertex v=s.nuThings.getVertices().get(0);
      MyPoint mp=new MyPoint(new Vec3D(v.xf(), v.yf(), v.zf()), v.getKey());
      s.pts.add(mp);
      s.selection.clear();
      HE_VertexEdgeCirculator veCrc=new HE_VertexEdgeCirculator(v);
      while (veCrc.hasNext ()) {
        HE_Halfedge he=veCrc.next();
        long svk=he.getStartVertex().getKey();
        long fvk=he.getEndVertex().getKey();
        for (MyPoint p : s.pts) {
          if (p.myKey==svk) {
            for (MyPoint pp : s.pts) {
              if (pp.myKey==fvk) {
                VerletSpring cs=new VerletSpring(p, pp, (float)he.getLength()/6, 0.001);
                p.mySprings.add(cs);
                pp.mySprings.add(cs);
                world.addSpring(cs);
              }
            }
          }
        }
      }
    }
  } else {
    if (keyPressed&&key=='l'&&s.cKey>0) {
      for (MyPoint p : s.pts) {
        if (p.myKey==s.cKey) {
          if (p.isLocked()) {
            p.unlock();
          } else {
            p.lock();
          }
        }
      }
    }
  }
}