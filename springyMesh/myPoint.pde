class MyPoint extends VerletParticle {

  long myKey;
  AttractionBehavior myAttr;
  AttractionBehavior myRep;
  ArrayList<VerletSpring> mySprings = new ArrayList<VerletSpring>();



  MyPoint(Vec3D pos, long _key) {
    super(pos);
    world.addParticle(this);
    myAttr=new AttractionBehavior(this, 200, 0.025);
    myRep=new AttractionBehavior(this, 200, -0.035);
    world.addBehavior(myAttr);
    world.addBehavior(myRep);
    myKey=_key;
  }

  void strech() {
    for (VerletSpring cs : mySprings) {
      cs.setRestLength(cs.getRestLength()+2);
    }
  }
      void strech(float l) {
    for (VerletSpring cs : mySprings) {
      cs.setStrength(cs.getStrength()-l);
    }
  }
  
  void display(){
    if(this.isLocked()){//if the point is locked, display it as a big magenta dot
     strokeWeight(10);
     stroke(200,0,100);
     point(this.x,this.y,this.z);
    }
    
  }
}