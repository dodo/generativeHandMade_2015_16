ArrayList<Field> allFields=new ArrayList<Field>();

class Field {
  
  ArrayList<VerletParticle2D> myPoints = new ArrayList<VerletParticle2D>();
  ArrayList<VerletSpring2D> mySprings = new ArrayList<VerletSpring2D>();
  boolean isActive=true;
  boolean isClosed=false;

  Field(Vec2D initpoint) {
    VerletParticle2D p =new VerletParticle2D(initpoint);
    myPoints.add(p);
    world.addParticle(p);
    world.addBehavior(new AttractionBehavior(p, 100, -1.2f, 0.007f));
  }

  void addOne(Vec2D pt) {//add one segment to the field. a segment is simply a particle attached to the previous one with a spring
    VerletParticle2D prev = myPoints.get(myPoints.size()-1);
    VerletParticle2D p =new VerletParticle2D(pt);
    myPoints.add(p);
    world.addParticle(p);
    world.addBehavior(new AttractionBehavior(p, 100, -1.2f, 0.007f));
    VerletSpring2D cs=new VerletSpring2D(prev, p, 1, 0.005);
    mySprings.add(cs);
    world.addSpring(cs);
  }
  void closeLoop() {//close the loop by adding a spring between first and the most recent particle
    VerletParticle2D prev = myPoints.get(myPoints.size()-1);
    VerletParticle2D first = myPoints.get(0);
    VerletSpring2D cs=new VerletSpring2D(prev, first, 1, 0.005);
    mySprings.add(cs);
    world.addSpring(cs);
    isClosed=true;
  }

  void display() {
    strokeWeight(5);
    if (!rec) {
      for (int i=0; i<myPoints.size(); i++) {//display all particles as points, if locked turn red
        VerletParticle2D cp=myPoints.get(i);
        if (cp.isLocked()) {
          stroke(255, 0, 0);
        } else {
          stroke(0);
        }
        point(cp.x, cp.y);
      }
    }
    stroke(0);
    for (int i=0; i<mySprings.size(); i++) {//display all the springs as lines
      VerletSpring2D cs=mySprings.get(i);
      strokeWeight(1);
      line(cs.a.x, cs.a.y, cs.b.x, cs.b.y);
    }
  }
}