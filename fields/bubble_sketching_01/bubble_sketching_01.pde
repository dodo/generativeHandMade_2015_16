import toxi.physics2d.*;
import toxi.physics2d.behaviors.*;
import toxi.physics2d.constraints.*;
import toxi.geom.*;

import processing.pdf.*;

VerletPhysics2D world;

boolean update=true;

ArrayList<VerletParticle2D> toConnect = new ArrayList<VerletParticle2D>();

Field cf;

Vec2D mousePos;

boolean rec=false;

void setup() {
  size(1000, 1000, P3D);
  world=new VerletPhysics2D();
  world.setDrag(0.45f);
  world.setWorldBounds(new Rect(0, 0, width, height));
  //world.addBehavior(new GravityBehavior(new Vec2D(0, -0.25f)));
}


void draw() {
  if (update)world.update();  
  background(255);
  mousePos=new Vec2D(mouseX, mouseY);  
  if(!rec){
    for (Bind b : allBinds)b.display();
    for(VerletParticle2D p:toConnect){
      ellipse(p.x,p.y,20,20);
    }
  }
  for (Field f : allFields)f.display();
  if (rec) {
    endRecord();
    rec=false;
  }
}