ArrayList<Bind> allBinds = new ArrayList<Bind>();

class Bind extends VerletSpring2D {//binding element is a regular VerletSpring with some features added

  boolean selected =false;
  
  Bind(VerletParticle2D a, VerletParticle2D b) {
    super(a, b, 5, 0.003);
    world.addSpring(this);
  }

  void display() {
    Line2D l=new Line2D(a, b);
    Vec2D midpoint = l.getMidPoint();
    stroke(200);
    if (midpoint.distanceTo(mousePos)<15) {
      stroke(200, 0, 0);
      selected=true;
    }
    else{
     selected=false; 
    }
    strokeWeight(1);    
    ellipse(a.x, a.y, 10, 10);
    ellipse(b.x, b.y, 10, 10);
    line(a.x, a.y, b.x, b.y);
  }
}