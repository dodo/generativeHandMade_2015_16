void mouseClicked() {
  if (keyPressed&&key=='a') {
    cf=new Field(mousePos);
    allFields.add(cf);
  } else if (cf!=null&&!cf.isClosed) {
    cf.addOne(mousePos);
  }
}

void mouseDragged(){
  if (cf!=null&&!cf.isClosed) {
    cf.addOne(mousePos);
  }
}

void keyPressed() {
  if (key==' ')update=!update;
  if (key=='c'&&cf!=null&&!cf.isClosed) {
    cf.closeLoop();
  }
  if (key=='l') {
    for (Field b : allFields) {
      for (int i=0; i<b.myPoints.size(); i++) {
        VerletParticle2D cp=b.myPoints.get(i);
        if (cp.distanceTo(mousePos)<10) {
          if (!cp.isLocked())cp.lock();
          else {
            cp.unlock();
          }
        }
      }
    }
  }

    if (key=='b') {
      for (Field b : allFields) {
        for (int i=0; i<b.myPoints.size(); i++) {
          VerletParticle2D cp=b.myPoints.get(i);
          if (cp.distanceTo(mousePos)<10) {
            if (toConnect.size()<2) {
              toConnect.add(cp);
            }
          }
        }
      }

      if (toConnect.size()==2) {
        allBinds.add(new Bind(toConnect.get(0), toConnect.get(1)));
        toConnect.clear();
      }
    }
    if(key=='x'){
     for(int i=0;i<allBinds.size();i++){
       Bind b=allBinds.get(i);
       if(b.selected){
         world.removeSpring(b);
         allBinds.remove(b);
       }
     }
    }
    if (key=='s') {
      beginRecord(PDF, "fields"+minute()+hour()+day()+month()+".pdf"); 
      rec=true;
    }
  }