import toxi.geom.*;
import toxi.geom.mesh.*;
import toxi.geom.mesh.subdiv.*;
import toxi.geom.mesh2d.*;
import toxi.math.*;
import toxi.math.conversion.*;
import toxi.math.noise.*;
import toxi.math.waves.*;
import toxi.util.*;
import toxi.util.datatypes.*;
import toxi.util.events.*;
import toxi.processing.*;

import java.util.List;

Voronoi v = new Voronoi();

ToxiclibsSupport gfx;

void setup() {
  size(1000, 1000); 
  gfx=new ToxiclibsSupport(this);
  background(255);
  smooth(32);
}

void draw() {
  background(255);

  List<Polygon2D> regions=v.getRegions();

  for (Polygon2D p : regions) {

    p.smooth(0.1, 0.01);
    strokeWeight(0.5);
    fill(250);
    stroke(0, 100);
    gfx.polygon2D(p);

    Vec2D centroid = p.getCentroid();
    //float circumference = p.getCircumference();
    strokeWeight(3);
    point(centroid.x, centroid.y);
  }
}

void mouseDragged() {
  v.addPoint(new Vec2D(mouseX+random(-10, 10), mouseY+random(-10, 10)));
}